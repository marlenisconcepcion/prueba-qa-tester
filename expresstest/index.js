const express = require('express');
const app = express();

app.use(express.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, access-token, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


app.get('/', function (req, res) {
  res.send({
    mensaje: 'Hello Word',
    status: 'gitlab',
    version: '1.0.0'
  });
});

app.get('/:id', function (req, res) {
    if(req.params.id){
        switch(req.params.id){
            case '1':
                res.send({nombre: 'Juan', apellido: 'Perez'});
                break;
            case '2':
                res.send({nombre: 'Pedro', apellido: 'Perez'});
                break;
            default:
                res.send({nombre: 'No encontrado', apellido: 'N/A'});
        }
    }
    res.send({status: 'Error'});
});

app.post('/', function (req, res) {
    if(req.body.user == 'admin' && req.body.pass == 'admin'){
        res.send({status: 'Ok'});
    }else{
        res.send({status: 'Error'});
    }
})

app.listen(5001, function () {
  console.log('App on port 5001');
});